# frozen_string_literal: true

control 'tmux-ng-package-install-git-pkg-installed' do
  title 'git should be installed'

  describe package('git') do
    it { should be_installed }
  end
end

control 'tmux-ng-package-install-pkg-installed' do
  title 'tmux should be installed'

  describe package('tmux') do
    it { should be_installed }
  end
end

control 'tmux-ng-package-install-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'tmux-ng-package-install-stock-plugins-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.tmux/plugins') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'tmux-ng-package-install-tpm-auser-installed' do
  title 'should exist'

  describe file('/home/auser/.tmux/plugins/tpm/tpm') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end
