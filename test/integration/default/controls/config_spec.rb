# frozen_string_literal: true

control 'tmux-ng-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/tmux') do
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'tmux-ng-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/tmux/tmux.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Set the prefix to ^s.') }
  end
end

control 'tmux-ng-config-file-config-symlink-auser-managed' do
  title 'should exist'

  describe file('/home/auser/.tmux.conf') do
    it { should be_symlink }
    it { should_not be_directory }
    its('link_path') { should eq '/home/auser/.config/tmux/tmux.conf' }
  end
end
