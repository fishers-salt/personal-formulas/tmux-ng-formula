# frozen_string_literal: true

control 'tmux-ng-package-clean-tpm-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.tmux/plugins/tpm') do
    it { should_not exist }
  end
end

control 'tmux-ng-package-clean-stock-plugins-dir-auser-absent' do
  title 'should not exist'
  describe directory('/home/auser/.tmux/plugins') do
    it { should_not exist }
  end
end

control 'tmux-ng-package-clean-pkg-absent' do
  title 'should not be installed'
  describe package('tmux') do
    it { should_not be_installed }
  end
end
