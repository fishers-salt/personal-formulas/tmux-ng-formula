# frozen_string_literal: true

control 'tmux-ng-config-file-config-symlink-auser-managed' do
  title 'should not exist'

  describe file('/home/auser/.tmux.conf') do
    it { should_not exist }
  end
end

control 'tmux-ng-config-file-config-auser-managed' do
  title 'should not exist'

  describe file('/home/auser/.config/tmux/tmux.conf') do
    it { should_not exist }
  end
end

control 'tmux-ng-config-file-config-dir-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/tmux') do
    it { should_not exist }
  end
end
