# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as tmux_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('tmux-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_tmux-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

tmux-ng-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/tmux
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - tmux-ng-package-install-user-{{ name }}-present

tmux-ng-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/tmux/tmux.conf
    - source: {{ files_switch([
                   name ~ '-tmux.conf.tmpl', 'tmux.conf.tmpl'],
                 lookup='tmux-ng-config-file-config-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - tmux-ng-config-file-config-dir-{{ name }}-managed

tmux-ng-config-file-config-symlink-{{ name }}-managed:
  file.symlink:
    - name: {{ home }}/.tmux.conf
    - target: {{ home }}/.config/tmux/tmux.conf
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - tmux-ng-config-file-config-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
