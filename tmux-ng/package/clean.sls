# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as tmux_ng with context %}

include:
  - {{ sls_config_clean }}

{% if salt['pillar.get']('tmux-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_tmux-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

tmux-ng-package-clean-tpm-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.tmux/plugins/tpm

tmux-ng-package-clean-stock-plugins-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.tmux/plugins
{% endif %}
{% endfor %}
{% endif %}

tmux-ng-pacakge-clean-pkg-absent:
  pkg.removed:
    - name: {{ tmux_ng.pkg.name }}
