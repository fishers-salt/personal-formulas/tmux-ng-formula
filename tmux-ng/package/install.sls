# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as tmux_ng with context %}

tmux-ng-package-install-git-pkg-installed:
  pkg.installed:
    - name: {{ tmux_ng.gitpkg.name }}

tmux-ng-pacakge-install-pkg-installed:
  pkg.installed:
    - name: {{ tmux_ng.pkg.name }}

{% if salt['pillar.get']('tmux-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_tmux-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

tmux-ng-package-install-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

tmux-ng-package-install-stock-plugins-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.tmux/plugins
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - tmux-ng-package-install-user-{{ name }}-present

tmux-ng-package-install-tpm-{{ name }}-installed:
  git.latest:
    - name: https://github.com/tmux-plugins/tpm
    - target: {{ home }}/.tmux/plugins/tpm
    - user: {{ name }}
    - require:
      - tmux-ng-package-install-stock-plugins-dir-{{ name }}-managed
      - tmux-ng-package-install-git-pkg-installed
      - tmux-ng-package-install-user-{{ name }}-present

{# tpm is cloned with ownership stooj:root #}
tmux-ng-package-install-tpm-ownership-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.tmux/plugins/tpm
    - user: {{ name }}
    - group: {{ user_group }}
    - recurse:
      - user
      - group
    - require:
      - tmux-ng-package-install-tpm-{{ name }}-installed
      - tmux-ng-package-install-user-{{ name }}-present

{% endif %}
{% endfor %}
{% endif %}
